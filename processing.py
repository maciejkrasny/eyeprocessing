import numpy as np
import cv2
import math


def algorythm(image):
    height = len(image)
    width = len(image[0])
    resultImage = image
    resultImage = cv2.medianBlur(resultImage,5)
    for x in range(1, height-1):
        for y in range(1, width-1):
            if checkseblings(image, x, y):
                resultImage[x][y] = [0,0,0]
                # mask[x][y] = 0
    return resultImage


def checkseblings(image, curHeight, curWidth):
    count = 0
    for i in range(-1,1):
        for j in range(-1,1):
            # print(w)
            if math.fabs(image[curHeight+i][curWidth+j][1]-image[curHeight][curWidth][1]) > 30:
                count = count + 1
            else:
                count = count - 1
    if count > 0:
        return True
    else:
        return False


def retouches(image):
    resultImage = image
    kernel = np.ones((5,5),np.uint8)
    resultImage = cv2.erode(resultImage,kernel,iterations = 1)
    kernel = np.ones((3,3),np.uint8)
    opening = cv2.morphologyEx(resultImage, cv2.MORPH_OPEN, kernel)
    kernel = np.ones((5,5),np.uint8)
    closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)
    return closing

def calc_average(image):
    avg_color_per_row = np.average(image, axis=0)
    avg_color = np.average(avg_color_per_row, axis=0)
    return avg_color






